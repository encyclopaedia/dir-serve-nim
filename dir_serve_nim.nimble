# Package

version = "0.0.1"
author = "Prashanth.Hegde"
description = "configurable http directory server"
license = "MIT"
srcDir = "src"
bin = @["dir_serve_nim"]


# Dependencies
requires "nim >= 1.6.10"
requires "parsetoml >= 0.6.0"
requires "nimja >= 0.8.6"

# tests
task tests, "Run all tests":
  exec "testament all"
