import main


doAssert hello(1) == 5

block:
  doAssert hello(3) == 7


doAssertRaises(ValueError):
  raise newException(ValueError, "specific errors")
