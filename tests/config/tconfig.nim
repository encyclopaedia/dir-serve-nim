import unittest
import config
import std / [strformat, sequtils, terminal]
import os

suite "test configuration parser":
  let configStr = joinPath(getCurrentDir(), "tests", "config", "config.toml")
  echo fmt"using configFile: {configStr}"

  test "test parse with default options":
    let opts = getAllConfigs(configStr)
    check opts.len == 2
    check opts.mapIt(it.name) == @["Movies", "Tmp"]

  test "test Movies with all optional params":
    let opts = getAllConfigs(configStr)
    let movies = opts[0]
    check movies.name == "Movies"
    check movies.path == "/Users/Z002DCM/dev/misc/dir-serve"
    check movies.upload == true
    check movies.recurse == true
    check movies.show_hidden == false
    check movies.allow_types == @[".v", ".js", ".css"]
    check movies.deny_types == newSeq[string]()

  test "test fetching relativePath from input":
    let opts = getAllConfigs(configStr)
    check opts.extractPath("Movies") == "/Users/Z002DCM/dev/misc/dir-serve"
    check opts.extractPath("Movies/one/two/three") == "/Users/Z002DCM/dev/misc/dir-serve/one/two/three"
    check opts.extractPath("Tmp") == "/tmp"
    check opts.extractPath("Tmp/one") == "/tmp/one"
    try:
      discard opts.extractPath("nonExistent/path")
      doAssert false, "nonExistent path is expected to error, but didn't"
    except:
      doAssert true

