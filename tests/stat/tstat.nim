import unittest
import stat, config
import std / [strformat, os]

suite "test stat":
  test "test human readable size":
    let testCases = [
      (999, "999"),
      (1040, "1K"),
      (1_200_000, "1M"),
    ]
    for testCase in testCases:
      let actualValue = testCase[0].bytesForHumans
      check actualValue == testCase[1]

  test "test filters for options in config file":
    let configStr = joinPath(getCurrentDir(), "tests", "stat", "config.toml")
    let opts = getAllConfigs(configStr)
    doAssert opts.len == 1
    let testData = [
      ("Test/one", 3),       # v, js and css shown
      ("Test/two", 2),       # dir and js shown
      ("Test/two/three", 0), # none present
    ]

    setCurrentDir(joinPath(getCurrentDir(), "tests", "stat"))
    for testCase in testData:
      let actual = opts.stat(testCase[0])
      check actual.len == testCase[1]

