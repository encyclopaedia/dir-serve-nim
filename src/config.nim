import std / [logging, sequtils, strutils, sugar, strformat]
import parsetoml
import os

type ServeOpts* = object
  name*: string
  path*: string
  upload*: bool
  recurse*: bool
  showHidden*: bool
  allowTypes*: seq[string]
  denyTypes*: seq[string]

proc extractPath*(opts: seq[ServeOpts], relativePath: string): string =
  assert relativePath.len > 0
  let shareNameWithPath = relativePath.split(DirSep, 1)
  info(fmt"shareNameWithPath = {shareNameWithPath}")
  let allOpts = opts.filterIt(it.name == shareNameWithPath[0])
  assert allOpts.len > 0
  let opt = allOpts[0]
  debug(fmt"opt = {opt}")
  return if shareNameWithPath.len > 1:
    joinPath(opt.path, shareNameWithPath[1])
  else:
    opt.path

proc getAllConfigs*(cfgFile: string): seq[ServeOpts] =
  let config = parsetoml.parseFile(cfgFile)
  let parseArrays = func(items: string): seq[string] =
    items.split(",")
      .mapIt(it.strip())
      .filterIt(it != "")
      .mapIt("." & it)

  let opts = config["shares"].getElems().map(share => ServeOpts(
    name: share["name"].getStr(),
    path: share["path"].getStr(),
    upload: try: share["upload"].getBool() except: false,
    recurse: try: share["recurse"].getBool() except: false,
    showHidden: try: share["show_hidden"].getBool() except: false,
    allowTypes: try: parseArrays(share["allow_types"].getStr("")) except: @[],
    denyTypes: try: parseArrays(share["deny_types"].getStr("")) except: @[],
  ))

  let invalidDenyTypes = opts.filterIt(it.allow_types.len > 0 and
      it.deny_types.len > 0)
  if invalidDenyTypes.len > 0:
    warn("both allow and deny types specified. deny types will be ignored")
  result = opts

proc getOptionsFromPath*(opts: seq[ServeOpts], relativePath: string): ServeOpts =
  let allOpts = if relativePath == "":
    info("relative path not provided, defaulting to the first option")
    opts
  else:
    #let shareNameWithPath = extractPath(opts, relativePath)
    let shareName = relativePath.split(DirSep, 1)[0]
    debug(fmt"shareName = {shareName}")
    opts.filterIt(it.name == shareName)
  assert allOpts.len > 0
  allOpts[0]
