import std / [os, sequtils, strformat, options, logging, mimetypes]
import config

type FileDetail* = tuple
  name: string
  typ: string
  size: string
  ext: string

let mimes = newMimetypes()
func bytesForHumans*(size: BiggestInt): string =
  let units = ["", "K", "M", "G", "T", "P", "E", "Z"]
  var num = (size)
  result = $size
  for unit in units:
    if num < 1000:
      result = fmt"{num}{unit}"
      break
    #num = cast[BiggestInt](cast[float64](num) / 1000)
    num = num /% 1000

proc serveFile*(opts: seq[ServeOpts], path: string): (string, string) =
  let synthesizedPath = extractPath(opts, path)
  debug(fmt"synthesized path = {synthesizedPath}")
  return if not fileExists(synthesizedPath):
    ("", "")
  else:
    let (_, name, ext) = splitFile(path)
    let contentType = mimes.getMimeType(ext)
    (contentType, readFile(path))

proc stat*(opts: seq[ServeOpts], path: string): seq[FileDetail] =
  if path.len == 0:
    return @[]
  let opt = getOptionsFromPath(opts, path)
  info(fmt"obtained {opt.name} option from specified path")
  let filterPath = proc (f: tuple[kind: PathComponent, path: string]): Option[FileDetail] =
    let fi = getFileInfo(f.path)
    let (_, name, ext) = splitFile(f.path)
    let fname = lastPathPart(f.path)
    let typ = case f.kind:
      of pcDir:
        "dir"
      of pcFile:
        "file"
      else:
        "other"
    let fd = (
      name: fname,
      typ: typ,
      size: bytesForHumans(fi.size),
      ext: ext,
    )

    return if not opt.recurse and f.kind == pcDir:
      debug(fmt"rejecting {name} due to recurse option")
      none[FileDetail]()
    elif opt.allowTypes.len > 0 and f.kind == pcFile and ext notin
        opt.allowTypes:
      debug(fmt"rejecting {name} due to allowTypes option")
      none[FileDetail]()
    elif not opt.showHidden and name[0] == '.':
      debug(fmt"rejecting {name} due to showHiden option")
      none[FileDetail]()
    else:
      some(fd)

  let resolvedPath = opts.extractPath(path)
  let files = toSeq(walkDir(resolvedPath))
  files
    .map(filterPath)
    .filterIt(isSome(it))
    .mapIt(it.get)

