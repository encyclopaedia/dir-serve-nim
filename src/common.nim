import std / [logging, colors]

func coloredLogOutput(lvl: Level, msg: string): string =
  return case lvl
  of lvlDebug: "\e[32m" & msg & "\e[0m"
  of lvlInfo: "\e[32m" & msg & "\e[0m"
  of lvlWarn: "\e[32m" & msg & "\e[0m"
  of lvlError: "\e[32m" & msg & "\e[0m"
  of lvlFatal: "\e[32m" & msg & "\e[0m"
  else: "\e[32m" & msg & "\e[0m"


#let logLevel* = lvlInfo
#let logger* = newConsoleLogger(levelThreshold=logLevel, fmtStr="[$datetime] - {coloredLogOutput($levelname, $levelname)}: ")
