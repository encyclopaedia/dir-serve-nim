import asynchttpserver, asyncdispatch
import nimja/parser
import logging
import stat, config
import std / [strformat, uri, sequtils, os]

### server
const templateFilePathRel = "./templates/root.html"

func renderPath(allPaths: seq[string], files: seq[FileDetail]): string =
  compileTemplateFile(getScriptDir / templateFilePathRel)

proc serve*(configFilePath: string) {.async.} =
  let server = newAsyncHttpServer()
  let opts = getAllConfigs(configFilePath)
  let allPaths = opts.mapIt(it.name)

  func extractPathFromReq(req: Request): string =
    let q = req.url.query.decodeQuery.toSeq.filterIt(it.key == "path")
    return if q.len == 0:
      ""
    else:
      q[0].value

  proc processRequest(req: Request) {.async.} =
    let path = extractPathFromReq(req)
    debug(fmt"extracted path = {path}")
    if path == "":
      return
    elif fileExists(extractPath(opts, path)):
      await req.respond(Http200, readFile(extractPath(opts, path)))
    else:
      let files = stat(opts, path)
      let headers = {"Content-Type": "text/html; charset=utf-8"}
      info(fmt"allpaths = {allPaths}")
      await req.respond(Http200, renderPath(allPaths, files), headers.newHttpHeaders())

  # server bootstrap
  server.listen Port(8080)
  while true:
    if server.shouldAcceptRequest():
      await server.acceptRequest(processRequest)
    else:
      poll()

