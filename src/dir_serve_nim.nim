import std / [parseopt, os, logging, strformat, asyncdispatch, ]
import config, serve

let logLevel* = lvlDebug
let logger = newConsoleLogger(levelThreshold = logLevel,
    fmtStr = "$datetime [$levelname] $app: ")
addHandler(logger)

func writeHelp(): string =
  return """
    Usage: dirserve <config_file>
      Flags and switches:
      -h | --help               : show help message
      -v | --version            : show version number
      -o | --output=file        : output file name
  """

func writeVersion(): string =
  return """
    version 0.0.1
  """

proc parseCommandArgs(): string =
  if paramCount() < 1:
    echo writeHelp()
    quit()
  for kind, key, val in getopt():
    case kind
    of cmdLongOption, cmdShortOption:
      case key
      of "help", "h":
        echo writeHelp()
        quit()
      of "version", "v":
        echo writeVersion()
        quit()
      else:
        error(fmt"unknown flag specified: {key}")
    of cmdArgument:
      if key == "":
        echo writeHelp()
        quit()
      result = key
    else:
      quit()

when isMainModule:
  let configFilePath = parseCommandArgs()
  asyncCheck serve(configFilePath)
  runForever()
  #if not fileExists(configFilePath):
  #  echo fmt"file {configFilePath} does not exist, please specify a valid filename"
  #  quit()
  #let opts = getAllConfigs(configFilePath)
  #let path = "Movies"
  #echo stat(opts, path)


